<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/about', [HomeController::class, 'index'])->name('about');

Route::get('/shop', [HomeController::class, 'index'])->name('shop');

Route::get('/blog', [HomeController::class, 'index'])->name('blog');

Route::get('/contact', [ContactController::class, 'index'])->name('contact');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'login'])->name('login-submit');

    Route::get('/register', [RegisterController::class, 'index'])->name('register');
    Route::post('/register', [RegisterController::class, 'register'])->name('register-submit');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

    Route::get('/cabinet', [UserController::class, 'index'])->name('cabinet');

    Route::get('/article', [ArticleController::class, 'create']);

    Route::post('/article', [ArticleController::class, 'save'])->name('article.save');

    Route::get('/edit_article/{id}', [ArticleController::class, 'edit']);

    Route::post('/edit_article', [ArticleController::class, 'saveEdit'])->name('article.edit');
});
