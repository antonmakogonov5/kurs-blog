@extends('layouts.app')

@section('content')
    <form action="{{route('register-submit')}}" method="post">
        @csrf

        <p>
            <label>
                Name
                <input type="text" name="name" required>
            </label>
        </p>

        <p>
            <label>
                Email
                <input type="email" name="email" required>
            </label>
        </p>

        <p>
            <label>
                Password
                <input type="password" name="password" required>
            </label>
        </p>

        <p>
            <label>
                Password confirmation
                <input type="password" name="password_confirmation" required>
            </label>
        </p>

        <button type="submit">Register</button>
    </form>

    <a href="{{route('login')}}">Already have account?</a>
@endsection
