@extends('layouts.app')

@section('content')
<article>
    <form method="POST" action="{{route('article.edit')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$article->id}}">
        <img src="/storage/{{$article->banner}}">
        <header class="entry-header">
            <h1 class="entry-title">Title</h1>
            <h1 class="entry-title"><input type="text" name="title" value="{{$article->title}}"></h1>
            <div class="entry-thumbnail">
                <h1 class="entry-title">Upload image</h1>
                <input type="file" name="banner">
            </div>
        </header>
        <!-- .entry-header -->
        <div class="entry-content">
            <textarea rows="6" name="content">{{$article->content}}</textarea>
        </div>
        <!-- .entry-content -->
        <footer class="entry-footer">
            <h1 class="entry-title">Select tags</h1>

            @foreach($tags as $tag)
            @php
            $result = $article->tags->first(function ($value, $key) use($tag) {
                return $value->id === $tag->id;
            });
      
            @endphp
            <div>
                <input type="checkbox"
                       @if($result !== null)
                       checked="true" 
                       @endif
                       id="{{$tag->name}}"
                       name="tags[]"
                       value="{{$tag->id}}"
                       >
                <label for="{{$tag->name}}">{{$tag->name}}</label>
            </div>
            @endforeach
        </footer>
        <!-- .entry-footer -->
        <button class="wpcmsdev-button color-green hentry" type="submit">Create</button>
    </form>
</article>
@endsection
