<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:5500',
            'banner' => 'required|string|max:255',
            'tags' => 'required|array',
            'tags.*' => 'required|integer'
        ];
    }
    
    public function messages() {
        return [
            'title.required' => 'Поле обязательно',
        ];
    }
}
