<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller {

    public function index() {
        $articles = Article::with('tags')
                ->where('user_id', Auth::id())
                ->get();


        return view('cabinet.index', ['articles' => $articles]);
    }
}
