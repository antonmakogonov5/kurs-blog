<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleSaveRequest;
use App\Models\Article;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class ArticleController extends Controller {

    public function create() {
        $tags = Tag::all();
        return view('cabinet.create_article', ['tags' => $tags]);
    }

    public function save(ArticleSaveRequest $request, Article $article) 
    {    
        $path = $request->file('banner')->store('banners');
        $article->saveArticle($request, $path);

        return redirect('/cabinet');
    }

    public function edit(Request $request) {
        $article = Article::findOrFail($request->id);
        $tags = Tag::all();
        return view('cabinet.get_article_for_edit',
                [
                    'article' => $article,
                    'tags' => $tags
        ]);
    }

    public function saveEdit(ArticleEditRequest $request) {
        $article = Article::findOrFail($request->id);
        $userId = Auth::id();
        $this->authorize('update', $article);
        
        if ($request->hasFile('banner')) {
            $pathNewFile = $request->file('banner')->store('banners');          
            $pathOldFile = $article->banner;
        }

        $article->saveArticle($request, $pathNewFile);
        if ($request->hasFile('banner')) {
            Storage::delete($pathOldFile);
        }

        return redirect('/cabinet');
    }

}
