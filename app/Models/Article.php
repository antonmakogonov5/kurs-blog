<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    protected $fillable = [
        'banner',
        'title',
        'content',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class, 'article_tags');
    }

    public function saveArticle(Request $request, string $path) 
    {
        try {
            DB::beginTransaction();
           
            $this->user_id = Auth::id();
            $this->title = $request->title;
            $this->content = $request->content;
            $this->banner = $path;
            $this->save();

            $this->tags()->sync($request->tags);
            DB::commit();
        } catch (Exception $ex) {
            Log::error($ex);
            DB::rollBack();
        }
    }

}
